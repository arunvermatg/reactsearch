import React, { Component } from 'react';

class Counter extends Component {

    state={
        count:this.props.value,
        items:[],
        isLoaded:false
        //tags :[]
    }

  
   
    // renderTags(){
    //     if(this.state.tags.length===0) return <p>There are no tags</p>;
    //     return <ul>{this.state.tags.map(tag=><li key={tag}>{tag}</li>)}</ul>;
    // }

    //FUNCTION IN JS ARE OBJECTS
    handleIncrement=()=>{
        console.log("Increment clicked",this);
        this.setState({count:this.state.count+1});
    }
    
    render() { 
        let classes = "badge m-2 ";
        classes+= this.state.count===0?"badge-warning":"badge-primary";

        console.log('props',this.props);

        return (
            <div>
                <h4>{this.props.id}</h4>
                <span  className={classes}>{this.formatCount()}</span>
                <button onClick={this.handleIncrement}
                         className='btn btn-secondary btn-sm'>Increment</button>
            </div>
        
        );
    }

    formatCount(){
        const {count}=this.state;
        return count==0?'Zero':count;
    }
}
 
export default Counter;