import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import UserForm from './components/UserForm';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

class App extends Component {

  state={
    users:[],
    activePage:0,
    count:0
  }
  

  handlePageChange = (activePage) =>{
    console.log(`Active page number is: ${activePage}`);
    this.setState({activePage});

    axios({ method: 'GET', url:`http://localhost:8983/solr/collection1/select?q=*%3A*&wt=json&indent=true&rows=10&start=${activePage}`, crossDomain: true }).then((res)=>{
      const users= res.data.response.docs;
      this.setState({users});
      console.log(users.length);
    })
  }

  getUser = (e) =>{
    e.preventDefault();
    
    const user = e.target.elements.username.value;
    const app= e.target.elements.application.value;
    axios({ method: 'GET', url:'http://localhost:8983/solr/collection1/select?q=*%3A*&wt=json&indent=true', crossDomain: true })
    .then((res)=>{
      console.log(res);
      const users= res.data.response.docs;
      this.setState({users});
      this.setState({count:res.data.response.numFound})
      console.log(users);
    })
  }
  render() {
    return (
      <div className="App">
        <UserForm getUser={this.getUser}/>        
        <div className="userResults">
        <div className="pages">
        {this.state.count > 10 ?
        <Pagination
          activePage={this.state.activePage}
          itemsCountPerPage={10}
          totalItemsCount={this.state.count}
          pageRangeDisplayed={5}
          onChange={this.handlePageChange}
        />:<p></p>}</div>
        <div className="results">
        {this.state.users.map((user, index) => (
        <p>Hello, {user.id} !</p>
    ))}
           </div>
         </div>
        
      </div>
    );
  }
}

export default App;
