import React, { Component } from 'react';
import '../userform.css';


const UserForm = (props) => {

    return(
        <div class="usersForm">
            <form onSubmit={props.getUser}>
                <div class="wrap">
                <div class="search">
                    <input type="text" name="username" class="username" placeholder="What you are looking for!!"/>
                    <select className="application" name="application">
                        <option value="gmp">GMP</option>
                        <option value="neo">NEO</option>
                        <option value="actimize">ACTIMIZE</option>
                    </select>
                    <button className="searchButton">Search</button>
                </div>
                </div>
            </form>
        </div>
    );
}

export default UserForm;